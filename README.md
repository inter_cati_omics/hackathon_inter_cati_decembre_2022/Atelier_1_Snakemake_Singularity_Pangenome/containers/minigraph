# minigraph Singularity container
### Package minigraph Version 0.20
3 tools: minigraph, vg and gfatools

Minigraph:

It is a sequence-to-graph mapper and graph constructor. For graph generation, it aligns a query sequence against a sequence graph and incrementally augments an existing graph with long query subsequences diverged from the graph. The figure on the right briefly explains the procedure.

Minigraph borrows ideas and code from minimap2. It is fairly efficient and can construct a graph from 90 human assemblies in a couple of days using 24 CPU cores. Older versions of minigraph was unable to produce base alignment. The latest version can. Please add option -c for graph generation as it generally improves the quality of graphs. (Li et al. Genome Biol, 10.1186/s13059-020-02168-z)

Proof-of-concept seq-to-graph mapper and graph generator

Homepage:

https://github.com/lh3/minigraph

VG:

For mapping genotyping using Giraffe (Siren et al. Science 10.1126/science.abg8871).

Variation graph data structures, interchange formats, alignment, genotyping, and variant calling methods

vg version 1.44

Homepage:

https://github.com/vgteam/vg

gfatools:

Tools for manipulating sequence graphs in the GFA and rGFA formats

version 0.5

Homepage:

https://github.com/lh3/gfatools


Homepage:

https://github.com/lh3/minigraph

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
minigraph Version: 0.20<br>
Singularity container based on the recipe: Singularity.minigraph_v0.20.def

Local build:
```
sudo singularity build minigraph_v0.20.sif Singularity.minigraph_v0.20.def
```

Get image help:
```
singularity run-help minigraph_v0.20.sif
```

Default runscript: minigraph<br>
Usage minigraph:
```
./minigraph_v0.20.sif
#or:
singularity exec minigraph_v0.20.sif minigraph
```

Usage vg:
```
singularity exec minigraph_v0.20.sif vg --help
```

Usage gfatools:
```
singularity exec minigraph_v0.20.sif gfatools
```



image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull minigraph_v0.20.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/minigraph/minigraph:latest

```

